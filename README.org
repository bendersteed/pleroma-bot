* Pleroma Bot
A simple bot for checking if there are new posts in my blog and then
posting an announcement in my pleroma account.

* Inspired by:
https://gkbrk.com/2018/08/mastodon-bot-in-common-lisp/

* Dependencies: 
      - [[https://github.com/Shinmera/tooter][tooter]]
      - [[https://edicl.github.io/drakma/][drakma]]
      - [[https://github.com/Shinmera/plump][plump]]

* How to run
First you need to get the appropriate secret, key and access-token for
initiating the client. These can be get like this:
- Run in sbcl:

#+BEGIN_SRC bash
(ql:quickload 'tooter)
(defvar *client* make-instance 'tooter:client
	:base "your instance url"
	:name "The name you want to give your client")
(tooter:authorize *client*)
#+END_SRC

- Then follow the link, login to your pleroma/mastodon instance and
  copy your access token.
- Now back in sbcl run:

#+BEGIN_SRC bash
(tooter:authorize *client* "the access token")
(tooter:secret *client*) ; your secret
(tooter:key *client*) ; your key
(tooter:access-token *client*) ;you access-token
#+END_SRC

- Copy these in the aforementioned order in a file, each in its own
  line, and then change the *key* var to show to your file.
- Set *blog* to the rss feed you want to track.
- Don't forget to put the right instance when initiating the client!
- Maybe also change the message with which you want to share the posts.

  After these configs you are ready to go. If you've put the files in
  a place where quicklisp can find them, maybe
  ~/quicklisp/local-projects/ then you can run in a terminal:
#+BEGIN_SRC bash
  buildapp --output pleroma-bot \
  --asdf-tree ~/quicklisp/ \
  --load-system pleroma-bot \
  --eval "(defun main (arg) (declare (ignore arg)) (pleroma-bot:run-bot))" \
  --entry main
#+END_SRC
to create a pleroma-bot stand-alone executable. For this you need [[https://www.xach.com/lisp/buildapp/][Buildapp]].

(asdf:defsystem pleroma-bot
  :version "0.1.0"
  :license "MIT"
  :components ((:file "package")
	       (:file "bot"))
  :depends-on (:drakma
	       :plump
	       :tooter))

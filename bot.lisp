(in-package #:pleroma-bot)

;; Setup client for fediverse. To get the credentials run
;; (tooter:authorize *client*) and then (tooter:authorize *client*
;; access-token) where access-token will be given to you if you login
;; in the web-page, then grab them and put them in a file you want
;; each in a new line, 1st secret, 2nd key, 3rd access-token

(defun get-credentials (file)
  "Returns the three first lines of a file, presuming them to be
secret, key and access-token, in that order, needed for authorizing
the bot"
  (with-open-file (in file)
    (values (read-line in)
	    (read-line in)
	    (read-line in))))

(defvar *key*
 "/home/bendersteed/quicklisp/local-projects/pleroma-bot/key")

(defvar *client*
  (multiple-value-bind (secret key access-token)
      (get-credentials *key*)
   (make-instance 'tooter:client
		  :base "https://pleroma.soykaf.com"
		  :name "Pleroma Bot"
		  :secret secret
		  :key key
		  :access-token access-token)))

;; Grab the list of posts from the blog's rss feed

(defvar *blog*
  "https://bendersteed.gitlab.io/post/index.xml")

(defstruct post title date url)

(defun find-first-element (tag node)
  "Search the XML node for the given tag name and return the text of the first one"
  (plump:render-text (car (plump:get-elements-by-tag-name node tag))))

(defun parse-rss-item (item)
  "Parse an RSS item into a lobsters-post"
  (let ((post (make-post)))
    (setf (post-title post) (find-first-element "title" item))
    (setf (post-date post) (find-first-element "pubDate" item))
    (setf (post-url post) (find-first-element "link" item))
    post))

(defun get-rss-feed ()
  "Get and parse the rss feed of blog into a list of posts"
  (let* ((xml-text (babel:octets-to-string (drakma:http-request *blog*)))
	 (plump:*tag-dispatchers* plump:*xml-tags*)
	 (xml-tree (plump:parse xml-text))
	 (items (plump:get-elements-by-tag-name xml-tree "item")))
    (map 'list #'parse-rss-item items)))

;; Share a post and record it

(defvar *links*
  "/home/bendersteed/quicklisp/local-projects/pleroma-bot/links.txt")

(defun share-post (item)
  "Take a blog post and post about it in Pleroma/Mastodon instance"
  (tooter:make-status *client*
		      (format nil "At ~A I published a new post in
		      my blog, titled: ~A. Read more here: ~A"
			      (post-date item)
			      (post-title item)
			      (post-url item))))

(defparameter *seen*
  (with-open-file (in *links* :if-does-not-exist :create)
    (loop for line = (read-line in nil) while line
       collect line)))

(defun record-post (item)
  "Writes a title to the links file to keep track of it"
  (with-open-file (stream *links*
                          :direction :output
                          :if-exists :append
                          :if-does-not-exist :create)
    (format stream "~a~%" (post-title item))))

(defun link-seen-p (item)
  "Check if the post in question is already posted"
  (member (post-title item) *seen* :test #'string-equal))

(defun run-bot ()
  (let* ((links (get-rss-feed))
	 (new-links (remove-if #'link-seen-p links)))
    (if (null new-links)
	(print "There aren't any new posts!")
	(progn
	  (print (concatenate 'string
			     "There are "
			     (format nil "~A" (length new-links))
			     "new posts"))
	  (loop for i in new-links
	     do (share-post i)
	       (record-post i))))))

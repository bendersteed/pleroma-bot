(defpackage :pleroma-bot
  (:use :cl)
  (:export #:run-bot)
  (:documentation "A simple bot for Mastodon/Pleroma that checks a
  blog for new posts and then posts them to fediverse"))
